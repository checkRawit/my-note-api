package main

import (
	"encoding/json"
	"net/http"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/cors"
)

func MakeRouter(noteHandler *NoteHandler) chi.Router {
	// Create router
	r := chi.NewRouter()

	r.Use(
		middleware.Logger,
		cors.Handler(cors.Options{
			AllowedMethods: []string{"GET", "POST", "PUT", "DELETE", "OPTIONS", "PATCH"},
			AllowedHeaders: []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
			ExposedHeaders: []string{"Link"},
			MaxAge:         300, // Maximum value not ignored by any of major browsers
		}),
		middleware.Timeout(60*time.Second),
		middleware.Recoverer,
	)

	r.Route("/_api", func(r chi.Router) {

		// Hello route
		r.Get("/", func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Content-Type", "application/json")
			if err := json.NewEncoder(w).Encode(struct {
				Message string `json:"message"`
			}{Message: "Hello World"}); err != nil {
				http.Error(w, "Error while encoding JSON", http.StatusInternalServerError)
			}
		})

		// User route
		r.Route("/users/me", func(r chi.Router) {
			// Auth token middleware
			r.Use(IDTokenMiddleware)

			r.Get("/", noteHandler.GETUser)
			r.Get("/notes", noteHandler.GETUserNotes)
		})

		// Notes route
		r.Route("/notes", func(r chi.Router) {
			// Auth token middleware
			r.Use(IDTokenMiddleware)

			r.Post("/", noteHandler.POSTUserNote)
			r.Route("/{noteID}", func(r chi.Router) {
				r.Delete("/", noteHandler.DELETEUserNote)
				r.Patch("/", noteHandler.PATCHUserNote)
			})
		})

		r.Route("/tags", func(r chi.Router) {
			r.Use(IDTokenMiddleware)

			r.Get("/", noteHandler.GETUserTags)
		})
	})

	return r
}
