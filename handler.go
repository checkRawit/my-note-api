package main

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"strconv"
	"strings"

	"github.com/go-chi/chi/v5"
	"gitlab.com/checkRawit/my-note-api/app"
)

type NoteHandler struct {
	service *app.Service
}

// NewNoteHandler create a new NoteHandler that inject Service into it
func NewNoteHandler(service *app.Service) NoteHandler {
	return NoteHandler{service}
}

func (handler NoteHandler) GETUser(w http.ResponseWriter, r *http.Request) {
	idToken := r.Context().Value("idToken").(string)
	user, err := handler.service.AuthenticateAndGetUser(idToken)
	if err != nil {
		switch {
		case errors.Is(err, app.ErrCannotDecodeIDToken):
			http.Error(w, "Invalid ID token", http.StatusUnauthorized)
		default:
			http.Error(w, "Internal Error", http.StatusInternalServerError)
		}
		return
	}

	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(user); err != nil {
		http.Error(w, "Internal Error", http.StatusInternalServerError)
		return
	}
}

func (handler NoteHandler) GETUserNotes(w http.ResponseWriter, r *http.Request) {
	filter := app.NoteFilter{
		Tag: r.URL.Query().Get("tag"),
	}
	if isArchived, err := strconv.ParseBool(r.URL.Query().Get("isArchived")); err == nil {
		filter.IsArchived = &isArchived
	}

	notes, err := handler.service.AuthenticateAndGetUserNotes(r.Context(), r.Context().Value("idToken").(string), filter)
	if err != nil {
		switch err {
		case app.ErrCannotDecodeIDToken:
			http.Error(w, "Cannot decode ID token", http.StatusUnauthorized)
		default:
			http.Error(w, "Cannot get user notes", http.StatusInternalServerError)
		}
		return
	}

	w.Header().Set("Content-Type", "application/json")
	if err = json.NewEncoder(w).Encode(notes); err != nil {
		http.Error(w, "Internal Error", http.StatusInternalServerError)
		return
	}
}

func (handler NoteHandler) POSTUserNote(w http.ResponseWriter, r *http.Request) {
	idToken := r.Context().Value("idToken").(string)
	var note struct {
		Message *string `json:"message"`
	}
	if err := json.NewDecoder(r.Body).Decode(&note); err != nil {
		http.Error(w, "Invalid body", http.StatusBadRequest)
		return
	}

	noteID, err := handler.service.AuthenticateAndCreateNote(r.Context(), idToken, *note.Message)
	if err != nil {
		http.Error(w, "Cannot create note", http.StatusInternalServerError)
		return
	}

	if err = json.NewEncoder(w).Encode(struct {
		ID string `json:"id"`
	}{noteID}); err != nil {
		http.Error(w, "Internal Error", http.StatusInternalServerError)
		return
	}
}

func (handler NoteHandler) DELETEUserNote(w http.ResponseWriter, r *http.Request) {
	idToken := r.Context().Value("idToken").(string)
	noteID := chi.URLParam(r, "noteID")

	if err := handler.service.AuthenticateAndDeleteNote(r.Context(), idToken, noteID); err != nil {
		http.Error(w, "Internal Error", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusNoContent)
	w.Write([]byte{})
}

func (handler NoteHandler) PATCHUserNote(w http.ResponseWriter, r *http.Request) {
	idToken := r.Context().Value("idToken").(string)
	noteID := chi.URLParam(r, "noteID")

	var note struct {
		Message    *string `json:"message"`
		IsArchived *bool   `json:"isArchived"`
	}

	if err := json.NewDecoder(r.Body).Decode(&note); err != nil {
		http.Error(w, "Invalid body", http.StatusBadRequest)
		return
	}

	if note.Message != nil {
		err := handler.service.AuthenticateAndUpdateNoteMessage(r.Context(), idToken, noteID, *note.Message)
		if err != nil {
			http.Error(w, "Cannot update message to the specific note", http.StatusInternalServerError)
			return
		}
	}
	if note.IsArchived != nil {
		err := handler.service.AuthenticateAndUpdateNoteIsArchived(r.Context(), idToken, noteID, *note.IsArchived)
		if err != nil {
			http.Error(w, "Cannot update isArchived to the specific note", http.StatusInternalServerError)
			return
		}
	}

	w.WriteHeader(http.StatusNoContent)
	w.Write([]byte{})
}

func (handler NoteHandler) GETUserTags(w http.ResponseWriter, r *http.Request) {
	filter := app.NoteFilter{}
	if isArchived, err := strconv.ParseBool(r.URL.Query().Get("isArchived")); err == nil {
		filter.IsArchived = &isArchived
	}

	tags, err := handler.service.AuthenticateAndGetUserAllTags(r.Context(), r.Context().Value("idToken").(string), filter)
	if err != nil {
		http.Error(w, "Cannot get user tags", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	if err = json.NewEncoder(w).Encode(struct {
		Tags []string `json:"tags"`
	}{Tags: tags}); err != nil {
		http.Error(w, "Internal Error", http.StatusInternalServerError)
		return
	}
}

func IDTokenMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authorizationHeader := r.Header.Get("Authorization")
		if authorizationHeader == "" {
			http.Error(w, "Authorization was not provided in header", http.StatusUnauthorized)
			return
		}

		extractedToken := strings.Split(authorizationHeader, "Bearer ")
		if len(extractedToken) < 2 {
			http.Error(w, "Invalid bearer token", http.StatusUnauthorized)
			return
		}

		ctx := context.WithValue(r.Context(), "idToken", extractedToken[1])
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
