module gitlab.com/checkRawit/my-note-api

go 1.16

require (
	firebase.google.com/go/v4 v4.6.1
	github.com/go-chi/chi/v5 v5.0.7
	github.com/go-chi/cors v1.2.0
	github.com/google/uuid v1.3.0
	go.mongodb.org/mongo-driver v1.7.4
)
