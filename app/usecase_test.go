package app

import (
	"context"
	"errors"
	"reflect"
	"testing"
)

func TestCreateNewService(t *testing.T) {
	repository := MockRepository{notes: map[string]Note{}}
	idGenerator := MockIDGenerator{}
	timeGenerator := MockTimeGenerator{}
	authService := MockAuthService{}
	service := NewService(repository, authService, idGenerator, timeGenerator)
	t.Log(service)
}

func TestReadNotesEmpty(t *testing.T) {
	repository := MockRepository{notes: map[string]Note{}}
	idGenerator := MockIDGenerator{}
	timeGenerator := MockTimeGenerator{}
	authService := MockAuthService{}
	service := NewService(repository, authService, idGenerator, timeGenerator)
	notes, err := service.AuthenticateAndGetUserNotes(context.TODO(), mockValidIDToken, NoteFilter{})
	if err != nil {
		t.Error(err)
		return
	}
	if len(notes) != 0 {
		t.Error("Received notes is not empty")
	}
}

func TestCreateNote(t *testing.T) {
	repository := MockRepository{notes: map[string]Note{}}
	idGenerator := MockIDGenerator{}
	timeGenerator := MockTimeGenerator{}
	authService := MockAuthService{}
	service := NewService(repository, authService, idGenerator, timeGenerator)

	expectedNote := Note{
		ID:              "1",
		Message:         "My added note",
		IsArchived:      false,
		UserUID:         mockValidIDToken,
		CreateTimeEpoch: mockTime,
		UpdateTimeEpoch: &mockTime,
		Tags:            make([]string, 0),
	}
	noteID, err := service.AuthenticateAndCreateNote(context.TODO(), mockValidIDToken, expectedNote.Message)
	if err != nil {
		t.Error(err)
		return
	}

	// Test assertion
	actualNote, ok := repository.notes[noteID]
	if !ok {
		t.Error("Note not found in Repository")
		return
	}

	if !reflect.DeepEqual(expectedNote, actualNote) {
		t.Error("Not the same note")
		return
	}

	t.Log(expectedNote, actualNote)
}

func TestDeleteNote(t *testing.T) {
	expectedNote := Note{
		ID:              "1",
		Message:         "My added note",
		IsArchived:      false,
		UserUID:         mockValidIDToken,
		CreateTimeEpoch: mockTime,
	}
	repository := MockRepository{notes: map[string]Note{expectedNote.ID: expectedNote}}
	idGenerator := MockIDGenerator{}
	timeGenerator := MockTimeGenerator{}
	authService := MockAuthService{}
	service := NewService(repository, authService, idGenerator, timeGenerator)

	err := service.AuthenticateAndDeleteNote(context.TODO(), mockValidIDToken, expectedNote.ID)
	if err != nil {
		t.Error(err)
		return
	}

	// Test assertion
	if _, ok := repository.notes[expectedNote.ID]; ok {
		t.Error("Note found in Repository")
		return
	}

	if len(repository.notes) != 0 {
		t.Error("notes is not empty")
	}

	t.Log(repository.notes)
}

// ------------------------------------ MOCK --------------------------------

const mockValidIDToken = "1"

var mockTime = 999

// MockIDGenerator is IDGenerator that only generate "1"
type MockIDGenerator struct{}

func (g MockIDGenerator) Generate() string {
	return "1"
}

// MockTimeGenerator is TimeGenerator that only returned value is constant
type MockTimeGenerator struct {
}

func (MockTimeGenerator) NowUnixMilli() int {
	return mockTime
}

// MockAuthService is AuthService that have only one user
type MockAuthService struct{}

func (a MockAuthService) DecodeIDToken(token string) (User, error) {
	if token != "1" {
		return User{}, errors.New("wrong idToken")
	}

	return User{UID: "1", DisplayName: "Check"}, nil
}

// MockRepository is Repository that use map[string]Note to store notes
type MockRepository struct {
	notes map[string]Note
}

func (r MockRepository) CreateUserNote(_ context.Context, note Note) error {
	if _, ok := r.notes[note.ID]; ok {
		return errors.New("exist note ID")
	}
	r.notes[note.ID] = note
	return nil
}

func (r MockRepository) ReadUserNotes(_ context.Context, userUID string, filter NoteFilter) ([]Note, error) {
	notes := []Note{}
	for _, note := range r.notes {
		if note.UserUID == userUID {
			notes = append(notes, note)
		}
	}
	return notes, nil
}

func (r MockRepository) ReadUserFilteredNotes(_ context.Context, userUID string, isArchived bool, tag string) ([]Note, error) {
	notes := []Note{}
	for _, note := range r.notes {
		if note.UserUID == userUID && note.IsArchived == isArchived {
			notes = append(notes, note)
		}
	}
	return notes, nil
}

func (r MockRepository) ReadNote(_ context.Context, noteID string) (Note, error) {
	note, ok := r.notes[noteID]
	if !ok {
		return Note{}, errors.New("note not found")
	}
	return note, nil
}

func (r MockRepository) UpdateNoteMessage(_ context.Context, noteID string, message string, updateTimeEpoch int, tags []string) error {
	note, ok := r.notes[noteID]
	if !ok {
		return errors.New("note not found")
	}
	note.Message = message
	note.UpdateTimeEpoch = &updateTimeEpoch
	note.Tags = tags
	r.notes[noteID] = note
	return nil
}

func (r MockRepository) UpdateNoteIsArchived(_ context.Context, noteID string, isArchived bool, updateTimeEpoch int) error {
	note, ok := r.notes[noteID]
	if !ok {
		return errors.New("note not found")
	}
	note.IsArchived = isArchived
	note.UpdateTimeEpoch = &updateTimeEpoch
	r.notes[noteID] = note

	return nil
}

func (r MockRepository) DeleteNote(_ context.Context, noteID string) error {
	if _, ok := r.notes[noteID]; !ok {
		return errors.New("not exist Note ID")
	}
	delete(r.notes, noteID)
	return nil
}

func (r MockRepository) ReadUserTags(ctx context.Context, userUID string, filter NoteFilter) ([]string, error) {
	panic("implement me")
}
