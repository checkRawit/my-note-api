package app

import (
	"time"

	"github.com/google/uuid"
)

type IDGeneratorer interface {
	Generate() string
}

type TimeGeneratorer interface {
	NowUnixMilli() int
}

type UUIDGenerator struct{}

func (UUIDGenerator) Generate() string {
	return uuid.NewString()
}

type TimeGenerator struct{}

func (TimeGenerator) NowUnixMilli() int {
	return int(time.Now().UnixNano() / 1e6)
}
