package app

import "context"

type Repositorier interface {
	CreateUserNote(context.Context, Note) error
	ReadUserNotes(ctx context.Context, userUID string, filter NoteFilter) ([]Note, error)
	ReadUserTags(ctx context.Context, userUID string, filter NoteFilter) ([]string, error)
	ReadNote(ctx context.Context, noteID string) (Note, error)
	UpdateNoteMessage(ctx context.Context, noteID string, message string, updateTimeEpoch int, tags []string) error
	UpdateNoteIsArchived(ctx context.Context, noteID string, isArchived bool, updateTimeEpoch int) error
	DeleteNote(ctx context.Context, noteID string) error
}
