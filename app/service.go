package app

import (
	"context"
	"fmt"
	"time"

	firebase "firebase.google.com/go/v4"
	"firebase.google.com/go/v4/auth"
)

// AuthServicer is an interface for authentication service
type AuthServicer interface {
	DecodeIDToken(token string) (User, error)
}

// FirebaseAuthService is auth validator by firebase
type FirebaseAuthService struct {
	auth    *auth.Client
	timeout time.Duration
}

// NewFirebaseAuthService is function that create FirebaseAuthService
func NewFirebaseAuthService(projectID string) (FirebaseAuthService, error) {
	const timeout = 10 * time.Second

	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	app, err := firebase.NewApp(ctx, &firebase.Config{ProjectID: projectID})
	if err != nil {
		return FirebaseAuthService{}, fmt.Errorf("NewFirebaseAuthService: %w", err)
	}

	client, err := app.Auth(ctx)
	if err != nil {
		return FirebaseAuthService{}, fmt.Errorf("NewFirebaseAuthService: %w", err)
	}

	return FirebaseAuthService{auth: client, timeout: timeout}, nil
}

// DecodeIDToken is a function that decode idToken from firebase
func (v FirebaseAuthService) DecodeIDToken(idToken string) (User, error) {

	ctx, cancel := context.WithTimeout(context.Background(), v.timeout)
	defer cancel()

	// To checking if ID token is revoked, use VerifyIDTokenAndCheckRevoked()
	token, err := v.auth.VerifyIDToken(ctx, idToken)
	if err != nil {
		return User{}, err
	}

	userRecord, err := v.auth.GetUser(ctx, token.UID)
	if err != nil {
		return User{}, fmt.Errorf("FirebaseAuthService: DecodeIDToken: %w", err)
	}

	return User{UID: userRecord.UID, DisplayName: userRecord.DisplayName}, nil
}
