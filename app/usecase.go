package app

import (
	"context"
	"errors"
	"regexp"
)

// Error used by Service
var (
	ErrCannotDecodeIDToken  = errors.New("service cannot decode ID token")
	ErrCannotGetUserNote    = errors.New("service cannot get user's note")
	ErrCannotGetUserTags    = errors.New("service cannot get user's tags")
	ErrCannotGetNote        = errors.New("service cannot get note")
	ErrCannotCreateUserNote = errors.New("service cannot create user's note")
)

type Service struct {
	repository    Repositorier
	authService   AuthServicer
	idGenerator   IDGeneratorer
	timeGenerator TimeGeneratorer
}

func NewService(repository Repositorier, authService AuthServicer, idGenerator IDGeneratorer, timeGenerator TimeGeneratorer) *Service {
	return &Service{repository, authService, idGenerator, timeGenerator}
}

func (service Service) AuthenticateAndGetUser(idToken string) (User, error) {
	// Authentication
	user, err := service.authService.DecodeIDToken(idToken)
	if err != nil {
		return User{}, ErrCannotDecodeIDToken
	}

	return user, nil
}

func (service Service) AuthenticateAndGetUserNotes(ctx context.Context, idToken string, filter NoteFilter) ([]Note, error) {
	// Authentication
	user, err := service.authService.DecodeIDToken(idToken)
	if err != nil {
		return nil, ErrCannotDecodeIDToken
	}

	// Get all user's notes
	notes, err := service.repository.ReadUserNotes(ctx, user.UID, filter)
	if err != nil {
		return nil, ErrCannotGetUserNote
	}

	return notes, nil
}

func (service Service) AuthenticateAndCreateNote(ctx context.Context, idToken string, noteMessage string) (string, error) {
	// Authentication
	user, err := service.authService.DecodeIDToken(idToken)
	if err != nil {
		return "", ErrCannotDecodeIDToken
	}

	createTimeEpoch := service.timeGenerator.NowUnixMilli()

	// Create note
	note := Note{
		ID:              service.idGenerator.Generate(),
		Message:         noteMessage,
		IsArchived:      false,
		UserUID:         user.UID,
		CreateTimeEpoch: createTimeEpoch,
		UpdateTimeEpoch: &createTimeEpoch,
		Tags:            findTags(noteMessage),
	}
	if err := service.repository.CreateUserNote(ctx, note); err != nil {
		return "", ErrCannotCreateUserNote
	}

	return note.ID, nil // Return note ID
}

func (service Service) AuthenticateAndUpdateNoteMessage(ctx context.Context, idToken string, noteID string, noteMessage string) error {
	// Authentication
	user, err := service.authService.DecodeIDToken(idToken)
	if err != nil {
		return ErrCannotDecodeIDToken
	}

	// Get note
	note, err := service.repository.ReadNote(ctx, noteID)
	if err != nil {
		return ErrCannotGetNote
	}

	if note.UserUID != user.UID {
		return errors.New("user cannot update this note")
	}

	// TODO: update tags
	if err := service.repository.UpdateNoteMessage(ctx, noteID, noteMessage, service.timeGenerator.NowUnixMilli(), findTags(noteMessage)); err != nil {
		return errors.New("repository cannot update message to the specific note")
	}

	return nil
}

func (service Service) AuthenticateAndUpdateNoteIsArchived(ctx context.Context, idToken string, noteID string, isArchived bool) error {
	// Authentication
	user, err := service.authService.DecodeIDToken(idToken)
	if err != nil {
		return ErrCannotDecodeIDToken
	}

	// Get note
	note, err := service.repository.ReadNote(ctx, noteID)
	if err != nil {
		return ErrCannotGetNote
	}

	if note.UserUID != user.UID {
		return errors.New("user cannot update this note")
	}

	if err := service.repository.UpdateNoteIsArchived(ctx, noteID, isArchived, service.timeGenerator.NowUnixMilli()); err != nil {
		return errors.New("repository cannot update isArchived to the specific note")
	}

	return nil
}

func (service Service) AuthenticateAndDeleteNote(ctx context.Context, idToken string, noteID string) error {
	// Authentication
	user, err := service.authService.DecodeIDToken(idToken)
	if err != nil {
		return ErrCannotDecodeIDToken
	}

	// Get note
	note, err := service.repository.ReadNote(ctx, noteID)
	if err != nil {
		return ErrCannotGetNote
	}

	if note.UserUID != user.UID {
		return errors.New("user cannot remove this note")
	}

	if err := service.repository.DeleteNote(ctx, noteID); err != nil {
		return errors.New("repository cannot remove the specific note")
	}

	return nil
}

func (service Service) AuthenticateAndGetUserAllTags(ctx context.Context, idToken string, filter NoteFilter) ([]string, error) {
	// Authentication
	user, err := service.authService.DecodeIDToken(idToken)
	if err != nil {
		return nil, ErrCannotDecodeIDToken
	}

	// Get all user's notes
	tags, err := service.repository.ReadUserTags(ctx, user.UID, filter)
	if err != nil {
		return nil, ErrCannotGetUserTags
	}

	return tags, nil
}

var tagPattern = regexp.MustCompile(`#(\w+)`)

func findTags(msg string) []string {

	matchs := tagPattern.FindAllStringSubmatch(msg, -1)

	tags := make([]string, len(matchs))
	for i, match := range matchs {
		tags[i] = match[1]
	}

	return tags
}

type NoteFilter struct {
	IsArchived *bool
	Tag        string
}
