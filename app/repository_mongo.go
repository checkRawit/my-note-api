package app

import (
	"context"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

type MongoRepository struct {
	client     *mongo.Client
	collection *mongo.Collection
	timeout    time.Duration
}

func NewMongoRepository(uri string) (*MongoRepository, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	client, err := mongo.Connect(ctx, options.Client().ApplyURI(uri))
	if err != nil {
		return nil, err
	}
	if err := client.Ping(ctx, readpref.Primary()); err != nil {
		return nil, err
	}

	return &MongoRepository{
		client: client,
		// TODO: database and collection name from config
		collection: client.Database("myNotes").Collection("note"),
		timeout:    30 * time.Second,
	}, nil
}

func (mr MongoRepository) CreateUserNote(ctx context.Context, note Note) error {
	res, err := mr.collection.InsertOne(ctx, note)
	if err != nil {
		return err
	}

	log.Printf("CreateUserNote: %s", res.InsertedID.(primitive.ObjectID).String())
	return nil
}

func (mr MongoRepository) ReadUserNotes(ctx context.Context, userUID string, filter NoteFilter) ([]Note, error) {
	opts := options.Find().SetSort(bson.M{"updateTimeEpoch": -1})

	findFilter := bson.M{"userUID": userUID}
	if filter.Tag != "" {
		findFilter["tags"] = filter.Tag
	}

	if filter.IsArchived != nil {
		findFilter["isArchived"] = *filter.IsArchived
	}

	cur, err := mr.collection.Find(ctx, findFilter, opts)
	if err != nil {
		return nil, err
	}
	defer cur.Close(ctx)

	notes := []Note{}
	if err := cur.All(ctx, &notes); err != nil {
		return nil, err
	}

	return notes, nil
}

func (mr MongoRepository) ReadUserTags(ctx context.Context, userUID string, filter NoteFilter) ([]string, error) {
	findFilter := bson.M{"userUID": userUID}
	if filter.IsArchived != nil {
		findFilter["isArchived"] = *filter.IsArchived
	}

	values, err := mr.collection.Distinct(ctx, "tags", findFilter)
	if err != nil {
		return nil, err
	}

	tags := make([]string, 0)
	for _, value := range values {
		tags = append(tags, value.(string))
	}

	return tags, nil
}

func (mr MongoRepository) ReadNote(ctx context.Context, noteID string) (Note, error) {
	var note Note
	if err := mr.collection.FindOne(ctx, bson.M{"noteID": noteID}).Decode(&note); err != nil {
		return Note{}, err
	}

	return note, nil
}

func (mr MongoRepository) UpdateNoteMessage(ctx context.Context, noteID string, message string, updateTimeEpoch int, tags []string) error {

	filter := bson.M{"noteID": noteID}
	update := bson.M{"$set": bson.M{"message": message, "updateTimeEpoch": updateTimeEpoch, "tags": tags}}
	return mr.collection.FindOneAndUpdate(ctx, filter, update).Decode(&Note{})
}

func (mr MongoRepository) UpdateNoteIsArchived(ctx context.Context, noteID string, isArchived bool, updateTimeEpoch int) error {

	filter := bson.M{"noteID": noteID}
	update := bson.M{"$set": bson.M{"isArchived": isArchived, "updateTimeEpoch": updateTimeEpoch}}
	return mr.collection.FindOneAndUpdate(ctx, filter, update).Decode(&Note{})
}

func (mr MongoRepository) DeleteNote(ctx context.Context, noteID string) error {

	filter := bson.M{"noteID": noteID}
	return mr.collection.FindOneAndDelete(ctx, filter).Decode(&Note{})
}

func (mr MongoRepository) Close(ctx context.Context) error {
	return mr.client.Disconnect(ctx)
}
