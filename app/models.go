package app

// Note is a note model
type Note struct {
	ID              string   `json:"id" bson:"noteID"`
	Message         string   `json:"message" bson:"message"`
	IsArchived      bool     `json:"is_archived" bson:"isArchived"`
	UserUID         string   `json:"user_uid" bson:"userUID"`
	CreateTimeEpoch int      `json:"create_time_epoch" bson:"createTimeEpoch"`
	UpdateTimeEpoch *int     `json:"update_time_epoch" bson:"updateTimeEpoch"`
	Tags            []string `json:"tags,omitempty" bson:"tags,omitempty"`
}

// User is a user model that is stored in firebase
type User struct {
	UID         string `json:"uid"`
	DisplayName string `json:"dispalyName"`
}
