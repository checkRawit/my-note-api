package main

import (
	"errors"
	"fmt"
	"os"
	"strconv"
)

// Config is a configuration for this application
type Config struct {
	FirebaseProjectID string
	DataSourceName    string
	Port              int
}

// LoadConfig create a configuration from environment variable
func LoadConfig() (*Config, error) {
	firebaseProjectID, ok := os.LookupEnv("FIREBASE_PROJECT_ID")
	if !ok {
		return nil, errors.New("FIREBASE_PROJECT_ID is not present in the environment")
	}

	dataSourceName, ok := os.LookupEnv("DATA_SOURCE_NAME")
	if !ok {
		return nil, errors.New("DATA_SOURCE_NAME is not present in the environment")
	}

	config := Config{
		Port:              8001,
		FirebaseProjectID: firebaseProjectID,
		DataSourceName:    dataSourceName,
	}

	if port, ok := os.LookupEnv("PORT"); ok {
		portNumber, err := strconv.Atoi(port)
		if err != nil {
			return nil, fmt.Errorf("failed to parse port from env to number: %w", err)
		}

		config.Port = portNumber
	}

	return &config, nil
}
