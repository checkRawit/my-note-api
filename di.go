package main

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/checkRawit/my-note-api/app"
)

func InitializeServer(config *Config) (*http.Server, func(), error) {
	authService, err := app.NewFirebaseAuthService(config.FirebaseProjectID)
	if err != nil {
		return nil, nil, fmt.Errorf("failed to init firebase auth service: %w", err)
	}

	repository, err := app.NewMongoRepository(config.DataSourceName)
	if err != nil {
		return nil, nil, fmt.Errorf("failed to init mongo repository: %w", err)
	}

	noteHandler := NewNoteHandler(
		app.NewService(
			repository,
			authService,
			app.UUIDGenerator{},
			app.TimeGenerator{},
		),
	)

	server := &http.Server{
		Addr:    fmt.Sprintf(":%d", config.Port),
		Handler: MakeRouter(&noteHandler),
	}

	cleanupFunc := func() {
		ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
		defer cancel()

		repository.Close(ctx)
	}

	return server, cleanupFunc, nil
}
